# API ioasys

Uma api desenvolvida para o teste de backend da ioasys.

# Rodar projeto

1. Criar um .env a partir do .env.example (usar mesmo PG_CONNECTION_STRING do .env.example)
2. Rodar os seguintes comandos
```bash
    > yarn
    > yarn dev
```

# Rodar testes

Apenas dois testes foram desenvolvidos como demonstração. Usar o seguinte comando para rodar os testes.

```bash
    > yarn test
```

# Rotas

Endpoints especificados [neste link](https://app.swaggerhub.com/apis/ccmoura/ioasys-api/1.0.0).

# Arquivo do Insomnia

Arquivo do insomnia pode ser baixado [aqui](https://drive.google.com/file/d/17mAfsXqA6RzIQHVEcHaUwsvG06eeXAro/view?usp=sharing) ou na pasta /insomnia do projeto.

# Banco de dados

Foi usada uma instância do postgre no [ElephantSQL](https://www.elephantsql.com). Modelagem do banco de dados pode ser encontrada [aqui](https://drive.google.com/file/d/114CBuEjaL1wOA3klPorgt9R3i7WeKS8C/view?usp=sharing)

# Outras informações

* Eu pretendia fazer um sistema de cache com redis mas ai precisaria rodar uma instância no docker para conseguir testar a aplicação (o mesmo para o banco de dados). Para manter o teste o mais simples possíve, deixei sem o sistema de cache e usei um banco de dados em nuvem.
* Pretendia também servir arquivos estáticos para inserir imagens de capas dos filmes, mas como não estava na especificação do problema acabei optando por não fazer.
* Alguns dados que eu usei para teste ainda podem estar no banco de dados.
* Para fazer essa api utilizei o node na versão 12.18.4.
* Utilizei uuid para os ids das tabelas por motivos de segurança (não representa o sentido de ordem dos dados nas tabelas como um id inteiro).

# Autor

* [Christopher Moura](https://www.linkedin.com/in/ccmoura/)
